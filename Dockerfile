FROM composer:1.8.5 as composer
COPY composer.* /app/
COPY package.json /app/
COPY yarn.lock /app/
COPY . /app
ARG ENVIRONMENT
RUN if [ "$ENVIRONMENT" = "dev" ]; then \
        set -xe && composer install --no-interaction --prefer-dist; \
        composer dump-autoload; \
    elif [ "$ENVIRONMENT" = "prod" ]; then \
        set -xe && composer install --no-dev --no-scripts --no-suggest --no-interaction --prefer-dist --optimize-autoloader; \
        composer dump-autoload --no-dev --optimize --classmap-authoritative; \
    fi
FROM php:7.3-fpm
WORKDIR /app
COPY . /app
COPY --from=composer /app/vendor /app/vendor
RUN if [ "$ENVIRONMENT" = "dev" ]; then \
        pecl install xdebug-2.7.2 && \
        docker-php-ext-enable xdebug && \
        echo "[XDEBUG]" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.profiler_enable=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.profiler_enable_trigger=1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.profiler_enable_trigger_value=active" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.profiler_output_dir=/home/trace" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.coverage_enable=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.default_enable=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.dump_globals=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.dump_once=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.extended_info=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
        echo "xdebug.collect_includes=0" >> /usr/local/etc/php/conf.d/xdebug.ini; \
    elif [ "$ENVIRONMENT" = "prod" ]; then \
        docker-php-ext-install opcache && \
        echo "opcache.memory_consumption=256" > /usr/local/etc/php/conf.d/opcache.ini && \
        echo "opcache.max_accelerated_files=20000" >> /usr/local/etc/php/conf.d/opcache.ini && \
        echo "opcache.validate_timestamps=0" >> /usr/local/etc/php/conf.d/opcache.ini && \
        echo "realpath_cache_size=4096K" >> /usr/local/etc/php/conf.d/opcache.ini && \
        echo "realpath_cache_ttl=600" >> /usr/local/etc/php/conf.d/opcache.ini; \
    fi
RUN apt-get update && apt-get install --no-install-recommends --assume-yes --quiet ca-certificates curl libpq-dev git libicu-dev libpng-dev libjpeg62-turbo-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl pdo pdo_pgsql pgsql gd \
    && apt-get clean && rm -rf /var/lib/apt/lists/* \
WORKDIR /var/www/symfony